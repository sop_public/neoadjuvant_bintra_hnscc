# Neoadjuvant bintrafusp alfa in patients with HNSCC

Single-cell RNA/TCR-seq data was generated during clinical trial NCT04247282 evaluating efficacy of neoadjuvant bintrafusp alfa in patients with newly diagnosed HPV-negative head and neck squamous cell carcinoma.
More specifically, tumor biopsies were obtained pre- and post-treatment. From these tumor biopsies T lymphocytes were isolated and subjected to combined single-cell RNA- and TCR-sequencing.

If you use the data in your research please cite: [Sievers et al, Cancer Cell 2023](https://pubmed.ncbi.nlm.nih.gov/37059104/); DOI: 10.1016/j.ccell.2023.03.014


## Description of the data and file structure

The three files contain the following information:


#### bintra-trial-scrna-data-seurat.rds

RDS file corresponding to a seurat object (please see R package 'Seurat' available on CRAN) containing single-cell RNA-seq data as obtained from cellranger (10x Genomics). This seurat object contains all cells prior to additional filtering that was applied in the corresponding publication (Sievers et al, Cancer Cell 2023; DOI: 10.1016/j.ccell.2023.03.014). Patient identity and treatment state (A = pre-treatment; B = post-treatment) are included as object metadata.

#### bintra-trial-cellranger-clonotypes.tsv 

TSV file containing descriptions of the identified clonotypes. This file was produced by cellranger vdj (10x Genomics).

#### bintra-trial-cellranger-contig-anno.tsv 

TSV file associating cell barcodes with clonotype identities.


For additional information please refer to [Sievers et al, Cancer Cell 2023](https://pubmed.ncbi.nlm.nih.gov/37059104/); DOI: 10.1016/j.ccell.2023.03.014


## Sharing/Access information

The unprocessed data is available on dbGaP using the accession number: [phs002849.v1.p1](https://www.ncbi.nlm.nih.gov/projects/gap/cgi-bin/study.cgi?study_id=phs002849.v1.p1)


## Code/Software

In it's current form the data is compatible with R.
